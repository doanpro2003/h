import React from "react";
import { Input, Space } from "antd";
const { Search } = Input;

const onSearch = (value) => console.log(value);
export default function SearchCourse() {
  return (
    <div>
      <Space direction="vertical">
        <Search
          className="bg-green-500 rounded-md md:w-20 lg:w-48"
          placeholder="Tìm Khóa Học"
          allowClear
          // enterButton="Search"
          size="middle "
          onSearch={onSearch}
        />
      </Space>
    </div>
  );
}
