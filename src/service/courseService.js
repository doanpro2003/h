// lấy danh sách khóa học
import { https } from "./configURL";
export const getCourse = () => {
  return https.get("/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01");
};
//lấy danh mục khóa học
export const getportCourse = () => {
  return https.get(`/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc`);
};

// lấy khóa học theo danh mục
export const getCourseByCate = (id) => {
  return https.get(
    `/api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc?maDanhMuc=${id}&MaNhom=GP01`
  );
};
// lấy thông tin khóa học
export const getDetailCourse = (id) => {
  return https.get(`/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${id}`);
};
// đăng ký khóa học
export const postRegisCourse = (data) => {
  return https.post(`/api/QuanLyKhoaHoc/DangKyKhoaHoc`, data);
};
//tìm kím khóa học
export const getFindCourse = (name) => {
  return https.get(
    `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?tenKhoaHoc=${name}&MaNhom=GP01`
  );
};
//Hủy đăng ký
export const postDeleteCourse = (data) => {
  return https.post(`/api/QuanLyKhoaHoc/HuyGhiDanh`, data);
};
//lấy danh sách khóa học chưa ghi danh
export const postNotRegis = (tk) => {
  return https.post(
    `/api/QuanLyNguoiDung/LayDanhSachKhoaHocChuaGhiDanh?MaNhom=GP01?TaiKhoan=${tk}`
  );
};
//ghi danh khóa học cho người dùng
export const postRegisCourseUser = (data) => {
  return https.post(`/api/QuanLyKhoaHoc/GhiDanhKhoaHoc`, data);
};
//lấy danh sách khóa học mà người dùng đó chờ xét duyệt
export const postWaitCourse = (tk) => {
  return https.post(`/api/QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet`, tk);
};
// lấy danh sách khóa học mà người dùng đã ghi danh
export const postReadyCours = (tk) => {
  return https.post(`/api/QuanLyNguoiDung/LayDanhSachKhoaHocDaXetDuyet`, tk);
};
// Thêm Khóa Học
export const postNewCourse = (data) => {
  return https.post(`/api/QuanLyKhoaHoc/ThemKhoaHoc`, data);
};
//xóa khóa học
export const deleteCourse = (MaKhoaHoc) => {
  return https.delete(`/api/QuanLyKhoaHoc/XoaKhoaHoc?maKhoaHoc=${MaKhoaHoc}`);
};
