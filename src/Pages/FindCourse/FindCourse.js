import React, { useState } from "react";
import { AudioOutlined } from "@ant-design/icons";
import { Button, Card, Input, message, Space } from "antd";
import { getFindCourse, postRegisCourse } from "../../service/courseService";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
const { Search } = Input;
const suffix = (
  <AudioOutlined
    style={{
      fontSize: 16,
      color: "#1890ff",
    }}
  />
);
export default function FindCourse() {
  const [courseArr, setcourseArr] = useState([]);
  console.log("courseArr", courseArr);
  const onSearch = (value) => {
    console.log("value", value);
    getFindCourse(value)
      .then((res) => {
        console.log(res);
        setcourseArr(res.data);
      })
      .catch((err) => {
        console.log(err);
        message.warning("Không tìm thấy tên này");
      });
  };
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  const renderFindCourse = () => {
    return courseArr.map((item) => {
      let axiosArr = {
        maKhoaHoc: item.maKhoaHoc,
        taiKhoan: user?.taiKhoan,
      };
      return (
        <Card
          className="shadow-2xl"
          hoverable
          cover={
            <img
              className="h-48 object-cover"
              alt="example"
              src={item.hinhAnh}
            />
          }
        >
          <p className="text-xl font-medium">{item.tenKhoaHoc}</p>

          <div>
            <div className="my-5">
              <p className="line-through">1.000.000đ</p>
              <p className="text-xl font-medium text-red-500">800.000đ</p>
            </div>
            <NavLink to={`/detail/${item.maKhoaHoc}`}>
              <button className="bg-red-500 mx-2 my-1 px-2 py-1 rounded-md text-white font-medium hover:brightness-75">
                {" "}
                xem chi tiết
              </button>
            </NavLink>
            <button
              className="bg-red-500 mx-2 my-1 px-2 py-1 rounded-md text-white font-medium hover:brightness-75"
              onClick={() => {
                postRegisCourse(axiosArr)
                  .then((res) => {
                    console.log(res);
                    message.success("Đăng ký thành công");
                  })
                  .catch((err) => {
                    console.log(err);
                    message.warning("bạn đã đăng ký khóa học này");
                  });
              }}
            >
              Đăng ký
            </button>
          </div>
        </Card>
      );
    });
  };
  return (
    <div className="pt-32 ">
      <Search
        style={{
          width: 500,
        }}
        className="bg-red-500 rounded-md  mb-10"
        placeholder="Vui lòng nhập từ khóa để tìm kiếm khóa học"
        allowClear
        enterButton="Tìm Kiếm"
        size="large"
        onSearch={onSearch}
      />
      <div className="  gap-5 px-20 lg:grid lg:grid-cols-4 lg:px-20 md:grid md:grid-cols-3 md:px-10">
        {" "}
        {renderFindCourse()}
      </div>
    </div>
  );
}
