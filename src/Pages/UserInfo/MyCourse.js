import React, { useEffect, useState } from "react";
import { message, Space, Table, Tag } from "antd";
import { postUserInfo } from "../../service/userService";
import { useSelector } from "react-redux";
import { postDeleteCourse } from "./../../service/courseService";
export default function MyCourse() {
  const columns = [
    {
      title: "Tên Khóa Học",
      dataIndex: "tenKhoaHoc",
      key: "tenKhoaHoc",
    },
    {
      title: "Hình Ảnh",
      dataIndex: "hinhAnh",
      key: "hinhAnh",
    },
    {
      title: "Đánh Giá",
      dataIndex: "danhGia",
      key: "danhGia",
    },
    {
      title: "Lượt Xem",
      dataIndex: "luotXem",
      key: "luotXem",
    },
    {
      title: "Mô Tả",
      dataIndex: "moTa",
      key: "moTa",
    },

    {
      title: "Hành Động",
      dataIndex: "hanhDong",
      key: "hanhDong",
    },
  ];

  const [UserInfo, setUserInfo] = useState([]);
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  console.log("UserInfo", UserInfo);
  let courseArr = () => {
    return UserInfo.chiTietKhoaHocGhiDanh?.map((item) => {
      let dataPostDelete = {
        taiKhoan: user.taiKhoan,
        maKhoaHoc: item.maKhoaHoc,
      };
      return {
        ...item,
        hanhDong: (
          <button
            onClick={() => {
              postDelete(dataPostDelete);
            }}
            className="bg-red-500 px-2 py-1 rounded-md hover:brightness-75"
          >
            xóa
          </button>
        ),
        hinhAnh: <img width={100} src={item.hinhAnh} alt="" />,
        moTa: (
          <p>
            {item.moTa?.length > 100
              ? item.moTa?.slice(0, 50) + "..."
              : item?.moTa}
          </p>
        ),
      };
    });
  };
  console.log("courseArr", courseArr());
  let postDelete = (dataPostDelete) => {
    postDeleteCourse(dataPostDelete)
      .then((res) => {
        console.log(res);
        message.success("xóa thành công");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("xóa thất bại");
      });
  };
  useEffect(() => {
    postUserInfo(user.taiKhoan)
      .then((res) => {
        // console.log(res);
        setUserInfo(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <Table scroll={{ x: 400 }} columns={columns} dataSource={courseArr()} />;
    </div>
  );
}
