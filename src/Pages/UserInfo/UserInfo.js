import { Tabs } from "antd";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { postUserInfo } from "../../service/userService";
import FormInfo from "./FormInfo";
import MyCourse from "./MyCourse";
export default function UserInfo() {
  const [UserInfo, setUserInfo] = useState([]);
  // let { taiKhoan, matKhau, hoTen, email, soDT } = UserInfo;
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  useEffect(() => {
    postUserInfo(user.taiKhoan)
      .then((res) => {
        // console.log(res);
        setUserInfo(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const onChange = (key) => {
    console.log(key);
  };

  const items = [
    {
      key: "1",
      label: `Thông tin cá nhân`,
      children: (
        <div>
          <FormInfo />
        </div>
      ),
    },
    {
      key: "2",
      label: `Khóa học của tôi`,
      children: (
        <div>
          <MyCourse />
        </div>
      ),
    },
  ];

  return (
    <div className="pt-32 px-5 lg:px-20 md:px-20 ">
      <Tabs
        className=""
        defaultActiveKey="1"
        items={items}
        onChange={onChange}
      />
    </div>
  );
}
