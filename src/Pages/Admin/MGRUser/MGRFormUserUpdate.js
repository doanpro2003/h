import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { setLoadingOff } from "../../../redux-toolkit/loadingSlice";
import { Button, Form, Input, message, Select } from "antd";
import { Option } from "antd/es/mentions";
import { LeftOutlined } from "@ant-design/icons";
import { useParams } from "react-router-dom";
import { getFindUser, putUserInfo } from "../../../service/userService";
import { userLocalStoreChange } from "../../../service/localService";

export default function MGRFormUserUpdate() {
  let dispatch = useDispatch();
  dispatch(setLoadingOff());
  const param = useParams();
  const UserUpdate = userLocalStoreChange.get();
  console.log("UserUpdate", UserUpdate);
  useEffect(() => {
    getFindUser(param.tk)
      .then((res) => {
        console.log(res.data);
        userLocalStoreChange.set(res.data);
        if (window.location.hash !== "#2") {
          window.location.href += "#2";
          window.location.reload(false);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const onFinish = (values) => {
    console.log("Success:", values);
    putUserInfo(values)
      .then((res) => {
        console.log(res);
        message.success("cập nhật thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("cập nhật thất bại");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div>
      {" "}
      <Form
        fields={[
          {
            name: ["taiKhoan"],
            value: UserUpdate[0]?.taiKhoan,
          },
          {
            name: ["matKhau"],
            value: UserUpdate[0]?.matKhau,
          },
          {
            name: ["hoTen"],
            value: UserUpdate[0]?.hoTen,
          },
          {
            name: ["soDT"],
            value: UserUpdate[0]?.soDt,
          },
          {
            name: ["maLoaiNguoiDung"],
            value: UserUpdate[0]?.maLoaiNguoiDung,
          },
          {
            name: ["maNhom"],
            value: UserUpdate[0]?.maNhom,
          },
          {
            name: ["email"],
            value: UserUpdate[0]?.email,
          },
        ]}
        className=""
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="tài Khoản"
          name="taiKhoan"
          rules={[
            {
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="mật khẩu"
          name="matKhau"
          rules={[
            {
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="họ tên"
          name="hoTen"
          rules={[
            {
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="số điện thoại"
          name="soDT"
          rules={[
            {
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="maLoaiNguoiDung"
          label="mã loại người dùng"
          rules={[{ required: true }]}
        >
          <Select
            placeholder="chọn loại người dùng"
            // onChange={onGenderChange}
            allowClear
          >
            <Option value="HV">Học Viên</Option>
            <Option value="GV">Giáo Vụ</Option>
          </Select>
        </Form.Item>
        <Form.Item name="maNhom" label="mã nhóm" rules={[{ required: true }]}>
          <Select
            placeholder="chọn nhóm người dùng"
            // onChange={onGenderChange}
            allowClear
          >
            <Option value="GP06">nhóm 6</Option>
          </Select>
        </Form.Item>
        <Form.Item
          label="email"
          name="email"
          rules={[
            {
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        ></Form.Item>
        <a className="flex justify-center items-center" href="/admin">
          <LeftOutlined />
          <LeftOutlined />
          trở lại
        </a>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button className="bg-green-500" htmlType="submit">
            Sửa
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
